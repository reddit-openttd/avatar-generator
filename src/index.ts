import jdenticon from 'jdenticon'
import querystring from 'querystring'

const valueOr = (val) => {
  return (val && val.value) || val;
};

const fromCfHeaders = (someArray) => {
  return Object.keys(someArray).reduce(
    (aggr, key) => ({
      [key]: valueOr(Array.isArray(someArray[key]) ? someArray[key][0] : someArray[key]),
      ...aggr,
    }),
    {},
  );
};

/**
 * @param {Record<string, string | string[]>} headers
 * @returns {Request}
 */
function toCfHeaders(headers) {
  return Object.keys(headers).reduce(
    (obj, key) => ({
      ...obj,
      [key]: [{key, value: headers[key]}],
    }),
    {},
  );
}

export async function handler(event, context) {
  const request = event.Records[0].cf.request;
  const host = event.Records[0].cf.config.distributionDomainName;

  try {
    if (request.uri === '/avatar') {
      const params = querystring.parse(request.querystring)
      const png = jdenticon.toPng(params.name || 'default', parseInt(params.size as string) || 128);
      return {
        body: png.toString('base64'),
        bodyEncoding: 'base64',
        status: '200',
        headers: {
          'content-type': [{ key: 'Content-Type', value: 'image/png' }]
        },
      };
    }
  } catch (e) {
    console.log('Error generating avatar: ' + (e.message || e.toString()));
    return {
      status: 500,
      statusDescription: 'Internal server error',
    };
  }

  return event; // Not handled, forward to origin.
}